import { writeFileSync } from "fs";
import path from "path";
import { Op } from "sequelize";
import { BelongsTo, Column, DataType, ForeignKey, HasMany, Model, Sequelize, Table } from "sequelize-typescript";

// Part 1

const filePathForTask1 = path.resolve(__dirname, "output", "output1.json");

const { PAIR, COURSE, AMOUNT } = process.env;

const task1 = () => {
  if (typeof PAIR !== "string" || PAIR.length === 0) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "Агрумент PAIR не был передан" }));
    return;
  }

  const courseParse = Number(COURSE);
  const amountParse = Number(AMOUNT);
  if (isNaN(courseParse)) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "Агрумент COURSE не был передан или некорректный" }));
    return;
  }

  if (isNaN(amountParse)) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "Агрумент AMOUNT не был передан или некорректный" }));
    return;
  }

  let result: number | null = null;

  switch (PAIR) {
    //Перевод долларов в рубли
    case "USDTORUB": {
      result = amountParse * courseParse;
      break;
    }

    //Перевод рублей в доллары
    case "RUBTOUSD": {
      result = amountParse / courseParse;
      break;
    }

    default: {
      writeFileSync(
        filePathForTask1,
        JSON.stringify({ message: "Агрумент PAIR некорректный. Допустимые значения: USDTORUB, RUBTOUSD" })
      );
      break;
    }
  }

  if (result === null) {
    return;
  }

  writeFileSync(
    filePathForTask1,
    JSON.stringify({
      input: {
        PAIR,
        COURSE,
        AMOUNT,
      },
      result,
    })
  );
};

task1();

// Part 2
const filePathForTask2 = path.resolve(__dirname, "output", "output2.json");

const initDB = async () => {
  @Table({})
  class Mark extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    lesson: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    fio: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    examDate: string;

    @Column({
      type: DataType.INTEGER,
      allowNull: false,
    })
    mark: number;

    @ForeignKey(() => Student)
    studentId: string;

    @BelongsTo(() => Student, {
      foreignKey: "studentId",
    })
    student!: Student;
  }

  @Table({})
  class Student extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    surname: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    name: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    birthdayDate: Date;

    @HasMany(() => Mark, {
      foreignKey: "studentId",
    })
    markList: Mark[];
  }

  const sequelize = new Sequelize({
    dialect: "postgres",
    host: "database",
    port: 5432,
    username: "postgres",
    password: "qwerty",
    database: "sachkov_practice_db",
  });

  sequelize.addModels([Student, Mark]);

  try {
    await sequelize.authenticate();
    console.log("Init sequelize OK");
  } catch (error) {
    console.error("Init sequelize error", error);
  }

  const studentList = await Student.findAll({
    include: [
      {
        model: Mark,
        required: true,
      },
    ],
  });

  const resultStudentList = studentList.filter(el => {
    const badMarkIndex = el.dataValues["markList"].findIndex((mark: any) => mark.dataValues['mark'] !== 5);
    return badMarkIndex === -1;
  })

  writeFileSync(filePathForTask2, JSON.stringify(resultStudentList));
};

initDB();
